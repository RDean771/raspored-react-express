import React, { Component } from 'react';
class LoginComponent extends Component {
  state = {
     korisnickoIme: '',
     lozinka:'',
  }
  handleKorisnickoImeChange = (event) => {
    this.setState({korisnickoIme: event.target.value});
  }
  handleLozinkaChange = (event) => {
    this.setState({lozinka: event.target.value});
  }
  render() {
    return (
      <div>
      <form>
        <label> Korisnicko ime </label>
        <br/>
        <input type="text" value={this.state.korisnickoIme} onChange={this.handleKorisnickoImeChange} name="Korisnicko Ime" />
        <br/>

        <label> Lozinka </label>
        <br/>
        <input type="text" value={this.state.lozinka} onChange={this.handleLozinkaChange} name="Lozinka" />
        <br/>
        <button type="submit"  onClick={(e)=>this.props.login(e,this.state.korisnickoIme,this.state.lozinka)} >Login</button>
        </form>
      </div>
       
    );
  }
}

export default LoginComponent;
