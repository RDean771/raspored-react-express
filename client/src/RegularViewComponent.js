import React, { Component } from 'react';
import axios from 'axios';
import {Navbar,NavbarBrand,NavbarHeader,Nav,NavItem,NavDropdown,MenuItem} from 'react-bootstrap';
class RegularViewComponent extends Component {
  state = {
    imenaStudija : [],
    godine : []
  }
  componentDidMount(){
    axios.get('http://localhost:3001/api/ucitajStudije')
    .then(res =>{
      this.setState({imenaStudija : res.data});
    }).then ( () => {
      this.state.imenaStudija.map((ime) => {
        axios.get(`/api/ucitajGodineStudija?imeStudija=${ime.ime_studija}`)
          .then( res => {
          this.setState( 
            {
              godine : [...this.state.godine, {
                "name": ime.ime_studija,
                "years": res.data.map(el => el.godina_studija)
              }]
            })
        })
      })
    })
   
}
  render() {
    return (
      <div>
      <Navbar className="my-navbar">
      <Nav className="my-navbar"> {this.state.godine.map((studij,index) => 
       <NavDropdown eventKey={index} title={studij.name}> {studij.years.map( (year) =>
      <MenuItem eventKey={index}> {year} </MenuItem>)}
      </NavDropdown>)
      }
      </Nav>
      </Navbar>
      <form onSubmit={this.handleSubmit}>
        <label> Ime Kolegija </label>
        <br/>

        <label> Datum zauzeća </label>
        <br/>

        <label> Vrijeme početka zauzeća </label>
        <br/>

        <label> Vrijeme kraja zauzeća </label>
        <br/>

        <label> Rezervacija predavaonice  </label>
        <br/>
        </form>
      </div>
       
    );
  }
}

export default RegularViewComponent;
