import React, { Component } from 'react';
import axios from 'axios';
class AdminPanelComponent extends Component {
  state = {
     korisnickoIme: '',
     staraLozinka:'',
     novaLozinka:'',
     potvrdiNovuLozinku:''
  }
  handleKorisnickoImeChange = (event) => {
    this.setState({korisnickoIme: event.target.value});
  }
  handleStaraLozinkaChange = (event) => {
    this.setState({staraLozinka: event.target.value});
  }
  handleNovaLozinkaChange = (event) => {
    this.setState({novaLozinka: event.target.value});
  }
  handlePotvrdiNovuLozinkuChange = (event) => {
    this.setState({potvrdiNovuLozinku: event.target.value});
  }
  change = (event) => {
    event.preventDefault();
    axios.post('http://localhost:3001/api/adminPanel',{
        Username: this.state.korisnickoIme,
        Password: this.state.staraLozinka,
        newPassword: this.state.novaLozinka,
        confirmNewPassword: this.state.potvrdiNovuLozinku
    }). then(res =>{
        console.log(res.data);
    })
  }
  render() {
    return (
      <div>
      <form>
        <label> Korisnicko Ime </label>
        <br/>
        <input type="text" value={this.state.korisnickoIme} onChange={this.handleKorisnickoImeChange} name="Korisnicko Ime" />
        <br/>
        <label> Stara lozinka </label>
        <br/>
        <input type="text" value={this.state.staraLozinka} onChange={this.handleStaraLozinkaChange} name="Lozinka" />
        <br/>

        <label> Nova lozinka </label>
        <br/>
        <input type="text" value={this.state.novaLozinka} onChange={this.handleNovaLozinkaChange} name="Nova lozinka" />
        <br/>

        <label> Potvrdi novu lozinku </label>
        <br/>
        <input type="text" value={this.state.potvrdiNovuLozinku} onChange={this.handlePotvrdiNovuLozinkuChange} name="Potvrdi novu lozinku" />
        <br/>
        <button type="submit"  onClick={(e)=>this.change(e)} >Promijeni</button>
        </form>
      </div>
       
    );
  }
}

export default AdminPanelComponent;
