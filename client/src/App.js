import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import AdminViewComponent from './AdminViewComponent';
import PrivateRoute from './PrivateRouteComponent';
import RegularViewComponent from './RegularViewComponent';
import { Route, Redirect } from 'react-router-dom'
import LoginComponent from './LoginComponent';
import AdminPanelComponent from './AdminPanelComponent';
class App extends Component {

  state = {
    authed: true,
    token : false
  }
  login = (event,korisnicko_ime,lozinka) => {
    event.preventDefault();
    axios.get(`http://localhost:3001/api/login?Username=${korisnicko_ime}&Password=${lozinka}`)
    .then(res => {
      this.setState({token: res.data.token});
      window.localStorage.setItem("rtk", this.state.token);
    });

  }
  render() {
    return (
      <div>
      <Route path='/' exact component={RegularViewComponent} />
      <Route exact path="/login" render={() => (
        this.state.authed ? (
          <Redirect to="/adminview"/>
        ) : (
          <LoginComponent login={this.login}/>
        )
      )}/>
      <PrivateRoute authed={this.state.authed} path='/adminview' component={AdminViewComponent} />
      <Route path='/adminpanel' exact component={AdminPanelComponent} />
      </div>
    );
  }
}

export default App;
