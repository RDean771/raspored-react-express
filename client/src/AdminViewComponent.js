import React, { Component } from 'react';
import axios from 'axios';
import {Navbar,Nav,NavDropdown,MenuItem} from 'react-bootstrap';
import './adminViewComponent.css';
class AdminViewComponent extends Component {
  state = {
    imeKolegija: '',
    Datum: '',
    Pocetak: '',
    Kraj: '',
    Predavaonica: '',
    slobodnePredavaonice: [],
    imenaStudija : [],
    godine : [],
    Token : ''
  }
  UNSAFE_componentWillMount(){
    axios.get(`http://localhost:3001/api/checkToken?Token=${window.localStorage.getItem('rtk')}`)
    .then(res => {
      console.log(res.data);
      if(res.data=="Unauthorized"){
        console.log('poz');
      }
    })
  }
  componentDidMount(){
    axios.get('http://localhost:3001/api/ucitajStudije')
    .then(res =>{
      this.setState({imenaStudija : res.data});
    }).then ( () => {
      this.state.imenaStudija.map((ime) => {
        axios.get(`/api/ucitajGodineStudija?imeStudija=${ime.ime_studija}`)
          .then( res => {
          this.setState( 
            {
              godine : [...this.state.godine, {
                "name": ime.ime_studija,
                "years": res.data.map(el => el.godina_studija)
              }]
            })
        })
      })
    })
   
  }
  handleKolegijChange = (event) => {
    this.setState({imeKolegija: event.target.value});
  }
  handleDatumChange = (event) => {
    this.setState({Datum: event.target.value});
  }
  handlePocetakChange = (event) => {
    this.setState({Pocetak: event.target.value});
  }
  handleKrajChange = (event) => {
    this.setState({Kraj: event.target.value});
  }
  handleRezervacijaChange = (event) => {
    this.setState({Predavaonica: event.target.value});
  }
  provjeriDostupnost = (event) =>{
    event.preventDefault();
    axios.get(`http://localhost:3001/api/dostupnostPredavanja?imeKolegija=${this.state.imeKolegija}&Datum=${this.state.Datum}&Pocetak=${this.state.Pocetak}&Kraj=${this.state.Kraj}`)
    .then(res =>{
      this.setState({ slobodnePredavaonice: res.data });
    });
  }
  rezerviraj = (event) =>{
    event.preventDefault();
    axios.post('http://localhost:3001/api/rezerviraj',{ //post parametri
      imeKolegija: this.state.imeKolegija,
      Datum: this.state.Datum,
      Pocetak: this.state.Pocetak,
      Kraj: this.state.Kraj,
      Predavaonica: this.state.Predavaonica,
      Token : window.localStorage.getItem('rtk')
    })
    .then(res =>{
      this.setState({asdf: 'Rezervirano!'});
    });
  }
  obrisi = (event) =>{
    event.preventDefault();
    axios.delete(`http://localhost:3001/api/obrisi?Predavaonica=${this.state.Predavaonica}&Datum=${this.state.Datum}&Pocetak=${this.state.Pocetak}&Kraj=${this.state.Kraj}
                  &Token=${window.localStorage.getItem('rtk')}`)
    .then(res =>{
      console.log();
    });
  }
  render() {
    return (
      <div>
      <Navbar className="my-navbar">
      <Nav className="my-navbar"> {this.state.godine.map((studij,index) => 
       <NavDropdown eventKey={index} title={studij.name}> {studij.years.map( (year) =>
      <MenuItem eventKey={index}> {year} </MenuItem>)}
      </NavDropdown>)
      }
      </Nav>
      </Navbar>
      <form>
        <div className="test">
        <label> Ime Kolegija </label>
        <br/>
        <input type="text" value={this.state.imeKolegija} onChange={this.handleKolegijChange} name="imeKolegija" />
        <br/>
      
        <label> Datum zauzeća </label>
        <br/>
        <input type="text" value={this.state.Datum} onChange={this.handleDatumChange} name="Datum" />
        <br/>
        
        <label> Vrijeme početka zauzeća </label>
        <br/>
        <input type="text" value={this.state.Pocetak} onChange={this.handlePocetakChange} name="Pocetak" />
        <br/>

        <label> Vrijeme kraja zauzeća </label>
        <br/>
        <input type="text" value={this.state.Kraj} onChange={this.handleKrajChange} name="Kraj" />
        <button type="button" onClick={(e) => this.provjeriDostupnost(e)} >Provjeri dostupnost</button>
        <br/>
        <label> Rezervacija predavaonice  </label>
        <br/>
        <input type="text" value={this.state.Predavaonica} onChange={this.handleRezervacijaChange} name="Predavaonica" />
        <button type="button"  onClick={(e)=>this.rezerviraj(e)} >Rezerviraj</button>
        <br/>
        <button type="button"  onClick={(e)=>this.obrisi(e)} >Obrisi unos</button>
        </div>
        </form>
      <ul>
      {this.state.slobodnePredavaonice.map(predavaonica => <li key={predavaonica.id}>{predavaonica.id}</li>)}
      </ul>
      </div>
       
    );
  }
}

export default AdminViewComponent;
