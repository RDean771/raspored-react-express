const mysql = require('mysql');
var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'raspored',
    multipleStatements: true
});

conn.connect((err) => {
    if(err){
        console.log('DB connection failed');
    } else{
        console.log('DB connection succeded');
    }
})
module.exports = conn;