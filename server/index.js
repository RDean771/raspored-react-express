//Mathos raspored
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const adminPanel=require('./api/adminPanel/adminPanel');
const dostupnostPredavanja=require('./api/dostupnost/dostupnostPredavanja');
const dostupnostVjezbi=require('./api/dostupnost/dostupnostVjezbi');
const login = require('./api/login/login');
const obrisi = require('./api/obrisi/obrisi');
const rezerviraj=require('./api/rezerviraj/rezerviraj');
const ucitajGodineStudija=require('./api/ucitajGodineStudija/ucitajGodineStudija');
const ucitajKalendar=require('./api/ucitajKalendar/ucitajKalendar');
const ucitajStudije=require('./api/ucitajStudije/ucitajStudije');
const checkToken = require('./api/checkToken/checkToken');


var app = express();
app.use(cors());
app.use(bodyParser.json());
app.use('/api/adminPanel',adminPanel);
app.use('/api/dostupnostPredavanja',dostupnostPredavanja);
app.use('/api/dostupnostVjezbi',dostupnostVjezbi);
app.use('/api/login',login);
app.use('/api/obrisi',obrisi);
app.use('/api/rezerviraj',rezerviraj);
app.use('/api/ucitajGodineStudija',ucitajGodineStudija);
app.use('/api/ucitajKalendar',ucitajKalendar);
app.use('/api/ucitajStudije',ucitajStudije);
app.use('/api/checkToken',checkToken);

app.listen(3001,()=>console.log('Express server is running on port 3001'));