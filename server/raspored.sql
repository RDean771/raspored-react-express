-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2018 at 04:02 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raspored`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `korisnicko_ime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `lozinka` varchar(128) COLLATE utf16_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`korisnicko_ime`, `lozinka`) VALUES
('Admin', '$2b$10$uVVSpOApoDtglbf7Cfm9K.AyTwaIldPMtMy7GBGbae5wxMwy5MT2u'),
('ljilja', '$2b$10$e8sLZtKXaaOjBbiVPllSruyZnIrgyFYLX7mzdtBviNrTD3r4fkW.q');

-- --------------------------------------------------------

--
-- Table structure for table `kolegij`
--

CREATE TABLE `kolegij` (
  `id` varchar(5) COLLATE utf16_croatian_ci NOT NULL,
  `ime_kolegija` varchar(50) COLLATE utf16_croatian_ci DEFAULT NULL,
  `broj_upisanih_studenata` int(11) NOT NULL,
  `broj_grupa_vjezbi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `kolegij`
--

INSERT INTO `kolegij` (`id`, `ime_kolegija`, `broj_upisanih_studenata`, `broj_grupa_vjezbi`) VALUES
('F003', 'Elektrodinamika', 40, 2),
('F004', 'Klasična mehanika I', 20, 1),
('F005', 'Klasična mehanika 2', 20, 1),
('F006', 'Uvod u kvantnu mehaniku', 20, 1),
('F007', 'Osnove fizike I', 20, 1),
('F008', 'Osnove fizike II', 20, 1),
('F009', 'Osnove fizike III', 20, 1),
('F010', 'Osnove fizike IV', 20, 1),
('F011', 'Praktikum iz osnova fizike A', 20, 1),
('F012', 'Praktikum iz osnova fizike B', 20, 1),
('I027', 'Matematički alati', 60, 4),
('I043', 'Bioinformatika', 20, 1),
('I044', 'Funkcijsko programiranje', 40, 2),
('I045', 'Moderni računalni sustavi', 20, 1),
('I046', 'Moderni sustavi baza podataka', 30, 2),
('I047', 'Natjecateljsko programiranje', 20, 1),
('I048', 'Objektno orijentirano programiranje', 60, 3),
('I049', 'Programiranje mobilnih aplikacija', 20, 1),
('I050', 'Računalne mreže', 20, 1),
('I051', 'Računalno jezikoslovlje', 20, 1),
('I052', 'Softversko inženjerstvo', 20, 1),
('I053', 'Strukture podataka i algoritmi I', 40, 2),
('I054', 'Strukture podataka i algoritmi II', 20, 1),
('I055', 'Ugrađeni sustavi', 20, 1),
('I056', 'Uvod u računalnu znanost', 100, 5),
('I057', 'Web programiranje', 30, 2),
('I058', 'Završni praktični projekt', 20, 1),
('I059', '3D računalna grafika', 20, 1),
('M009', 'Funkcije više varijabli', 120, 2),
('M015', 'Kompleksna analiza', 80, 2),
('M063', 'Primjene diferencijalnog i integralnog računa II', 40, 2),
('M083', 'Algebra', 60, 2),
('M084', 'Diferencijalni račun', 140, 3),
('M085', 'Integralni račun', 100, 3),
('M086', 'Linearna algebra I', 140, 3),
('M087', 'Linearna algebra II', 100, 3),
('M088', 'Matematička logika u računalnoj znanosti', 60, 3),
('M089', 'Numerička matematika', 60, 2),
('M090', 'Obične diferencijalne jednadžbe', 60, 2),
('M091', 'Primijenjena matematika za računalnu znanost', 40, 2),
('M092', 'Osnove teorije upravljanja s primjenama', 20, 1),
('M093', 'Primjene diferencijalnog i integralnog računa I', 40, 2),
('M094', 'Realna analiza', 60, 2),
('M095', 'Statistički praktikum', 60, 3),
('M096', 'Strojno učenje', 40, 2),
('M097', 'Teorijske osnove računalne znanosti', 40, 2),
('M098', 'Uvod u vjerojatnost i statistiku', 60, 2),
('M099', 'Vektorski prostori', 40, 2),
('M100', 'Elementarna matematika', 60, 2),
('M101', 'Elementarna geometrija', 60, 2),
('M102', 'Kombinatorna i diskretna matematika', 60, 2),
('M103', 'Matematička natjecanja', 20, 1),
('M104', 'Metode numeričke matematike', 20, 1),
('M105', 'Uvod u diferencijalnu geometriju', 20, 1),
('M106', 'Teorija skupova', 20, 1),
('M107', 'Metode matematičke fizike', 20, 1),
('M108', 'Teorija brojeva', 60, 2),
('Z007', 'Završni rad', 60, 1),
('Z011', 'Strani jezik u struci I', 100, 2),
('Z012', 'Strani jezik u struci II', 100, 2),
('Z013', 'Stručna praksa', 20, 1),
('Z014', 'Tjelesna i zdravstvena kultura I', 100, 3),
('Z015', 'Tjelesna i zdravstvena kultura II', 60, 3);

-- --------------------------------------------------------

--
-- Table structure for table `kolegij_predavaonica_zauzetost`
--

CREATE TABLE `kolegij_predavaonica_zauzetost` (
  `kolegij_id` varchar(5) COLLATE utf16_croatian_ci NOT NULL,
  `predavaonica_id` varchar(3) COLLATE utf16_croatian_ci NOT NULL,
  `zauzetost_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `kolegij_predavaonica_zauzetost`
--

INSERT INTO `kolegij_predavaonica_zauzetost` (`kolegij_id`, `predavaonica_id`, `zauzetost_id`) VALUES
('I044', 'D3', 13),
('I056', 'D1', 12),
('M084', 'D1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `kolegij_studij`
--

CREATE TABLE `kolegij_studij` (
  `kolegij_id` varchar(5) COLLATE utf16_croatian_ci NOT NULL,
  `studij_id` varchar(5) COLLATE utf16_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `kolegij_studij`
--

INSERT INTO `kolegij_studij` (`kolegij_id`, `studij_id`) VALUES
('F004', 'PM2'),
('F007', 'PM2'),
('F008', 'PM2'),
('I027', 'PM2'),
('I043', 'PMR2'),
('I044', 'PMR1'),
('I045', 'PMR2'),
('I046', 'PMR2'),
('I047', 'PMR2'),
('I048', 'PM2'),
('I048', 'PMR1'),
('I049', 'PMR2'),
('I050', 'PMR2'),
('I051', 'PMR2'),
('I053', 'PM2'),
('I053', 'PMR2'),
('I054', 'PMR2'),
('I055', 'PMR2'),
('I056', 'PM1'),
('I056', 'PMR1'),
('I059', 'PMR2'),
('M009', 'PM2'),
('M015', 'PM2'),
('M084', 'PM1'),
('M084', 'PMR1'),
('M085', 'PM1'),
('M085', 'PMR1'),
('M086', 'PM1'),
('M086', 'PMR1'),
('M087', 'PM1'),
('M087', 'PMR1'),
('M088', 'PM2'),
('M088', 'PMR1'),
('M089', 'PM2'),
('M091', 'PMR2'),
('M093', 'PM2'),
('M093', 'PMR2'),
('M095', 'PM2'),
('M097', 'PM2'),
('M097', 'PMR2'),
('M098', 'PM2'),
('M098', 'PMR2'),
('M099', 'PMR2'),
('M100', 'PM1'),
('M101', 'PM1'),
('M102', 'PM1'),
('M103', 'PM2'),
('M108', 'PM2'),
('Z011', 'PM1'),
('Z011', 'PMR1'),
('Z012', 'PM1'),
('Z012', 'PMR1'),
('Z014', 'PM1'),
('Z014', 'PMR1'),
('Z015', 'PM2'),
('Z015', 'PMR2');

-- --------------------------------------------------------

--
-- Table structure for table `predavaonica`
--

CREATE TABLE `predavaonica` (
  `id` varchar(3) COLLATE utf16_croatian_ci NOT NULL,
  `broj_mjesta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `predavaonica`
--

INSERT INTO `predavaonica` (`id`, `broj_mjesta`) VALUES
('D1', 110),
('D2', 70),
('D3', 60),
('D4', 45),
('D5', 40),
('D6', 37),
('D7', 25),
('D8', 40),
('D9', 35),
('RP1', 20),
('RP2', 19),
('RP3', 20);

-- --------------------------------------------------------

--
-- Table structure for table `studij`
--

CREATE TABLE `studij` (
  `id` varchar(5) COLLATE utf16_croatian_ci NOT NULL,
  `ime_studija` varchar(128) COLLATE utf16_croatian_ci NOT NULL,
  `godina_studija` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `studij`
--

INSERT INTO `studij` (`id`, `ime_studija`, `godina_studija`) VALUES
('DFMS1', 'Diplomski studij matematike - financijska matematika i statistika', 1),
('DFMS2', 'Diplomski studij matematike - financijska matematika i statistika', 2),
('DMR1', 'Diplomski studij matematike - matematika i računarstvo', 1),
('DMR2', 'Diplomski studij matematike - matematika i računarstvo', 2),
('NMI3', 'Nastavnički studij matematike i informatike', 3),
('NMI4', 'Nastavnički studij matematike i informatike', 4),
('NMI5', 'Nastavnički studij matematike i informatike', 5),
('PM1', 'Preddiplomski studij matematike', 1),
('PM2', 'Preddiplomski studij matematike', 2),
('PM3', 'Preddiplomski studij matematike', 3),
('PMR1', 'Preddiplomski studij matematike i računarstva', 1),
('PMR2', 'Preddiplomski studij matematike i računarstva', 2);

-- --------------------------------------------------------

--
-- Table structure for table `zauzetost`
--

CREATE TABLE `zauzetost` (
  `id` int(11) NOT NULL,
  `datum_zauzetosti` date NOT NULL,
  `vrijeme_pocetka_zauzetosti` time NOT NULL,
  `vrijeme_kraja_zauzetosti` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `zauzetost`
--

INSERT INTO `zauzetost` (`id`, `datum_zauzetosti`, `vrijeme_pocetka_zauzetosti`, `vrijeme_kraja_zauzetosti`) VALUES
(11, '2018-07-26', '08:00:00', '12:00:00'),
(12, '2018-07-26', '12:00:00', '14:00:00'),
(13, '2018-07-26', '10:00:00', '12:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`korisnicko_ime`,`lozinka`);

--
-- Indexes for table `kolegij`
--
ALTER TABLE `kolegij`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kolegij_predavaonica_zauzetost`
--
ALTER TABLE `kolegij_predavaonica_zauzetost`
  ADD PRIMARY KEY (`kolegij_id`,`predavaonica_id`,`zauzetost_id`),
  ADD KEY `predavaonica_id` (`predavaonica_id`),
  ADD KEY `zauzetost_id` (`zauzetost_id`);

--
-- Indexes for table `kolegij_studij`
--
ALTER TABLE `kolegij_studij`
  ADD PRIMARY KEY (`kolegij_id`,`studij_id`),
  ADD KEY `studij_id` (`studij_id`);

--
-- Indexes for table `predavaonica`
--
ALTER TABLE `predavaonica`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studij`
--
ALTER TABLE `studij`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zauzetost`
--
ALTER TABLE `zauzetost`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zauzetost`
--
ALTER TABLE `zauzetost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kolegij_predavaonica_zauzetost`
--
ALTER TABLE `kolegij_predavaonica_zauzetost`
  ADD CONSTRAINT `kolegij_predavaonica_zauzetost_ibfk_1` FOREIGN KEY (`kolegij_id`) REFERENCES `kolegij` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kolegij_predavaonica_zauzetost_ibfk_2` FOREIGN KEY (`predavaonica_id`) REFERENCES `predavaonica` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kolegij_predavaonica_zauzetost_ibfk_3` FOREIGN KEY (`zauzetost_id`) REFERENCES `zauzetost` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kolegij_studij`
--
ALTER TABLE `kolegij_studij`
  ADD CONSTRAINT `kolegij_studij_ibfk_1` FOREIGN KEY (`kolegij_id`) REFERENCES `kolegij` (`id`),
  ADD CONSTRAINT `kolegij_studij_ibfk_2` FOREIGN KEY (`studij_id`) REFERENCES `studij` (`id`),
  ADD CONSTRAINT `kolegij_studij_ibfk_3` FOREIGN KEY (`kolegij_id`) REFERENCES `kolegij` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kolegij_studij_ibfk_4` FOREIGN KEY (`studij_id`) REFERENCES `studij` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
