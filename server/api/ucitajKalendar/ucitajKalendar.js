const express= require('express');
const router = express.Router();
const conn = require('../../connection/connection');

router.get('/', (req, res) => {
    conn.query(`SELECT ime_kolegija, predavaonica_id, datum_zauzetosti, vrijeme_pocetka_zauzetosti, vrijeme_kraja_zauzetosti FROM 
                kolegij INNER JOIN kolegij_predavaonica_zauzetost ON kolegij.id=kolegij_predavaonica_zauzetost.kolegij_id INNER JOIN 
                zauzetost ON zauzetost.id = kolegij_predavaonica_zauzetost.zauzetost_id WHERE ime_kolegija IN(
                SELECT ime_kolegija FROM kolegij INNER JOIN kolegij_studij on kolegij.id = kolegij_studij.kolegij_id INNER JOIN 
                studij ON studij.id = kolegij_studij.studij_id WHERE ime_studija = "${req.query.imeStudija}" AND godina_studija=${req.query.Godina})`,(err,rows,fields)=>{
        if(err){
            throw err;
        } else{
            res.send(rows);
        }
    })
})

module.exports = router;