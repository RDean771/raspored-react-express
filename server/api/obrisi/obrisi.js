const express= require('express');
const router = express.Router();
const conn = require('../../connection/connection');
const bcrypt = require('bcrypt');

router.delete('/', (req, res) => {
    if(!req.query.Token)
        res.sendStatus(401);
    else{
        if(bcrypt.compareSync("helpmepls",req.query.Token)){
        conn.query(`DELETE FROM zauzetost WHERE id=(SELECT implicitTemp.id from (SELECT id FROM kolegij_predavaonica_zauzetost INNER JOIN 
                    zauzetost ON kolegij_predavaonica_zauzetost.zauzetost_id = zauzetost.id WHERE
                    predavaonica_id ="${req.query.Predavaonica}" AND datum_zauzetosti ="${req.query.Datum}" AND vrijeme_pocetka_zauzetosti="${req.query.Pocetak}"
                    AND vrijeme_kraja_zauzetosti="${req.query.Kraj}") implicitTemp)`,(err,rows,fields)=>{
            if(err){
                res.sendStatus('Error Occured');
            } else{
                res.send('Deleted');
            }
        })
        }
        else{
            res.sendStatus(401);
        }
    }
})

module.exports = router;