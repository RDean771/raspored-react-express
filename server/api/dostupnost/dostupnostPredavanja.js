const express= require('express');
const router = express.Router();
const conn = require('../../connection/connection');

router.get('/', (req, res) => {
    conn.query(`SELECT id from predavaonica WHERE 1.5*broj_mjesta >= (SELECT broj_upisanih_studenata FROM kolegij WHERE ime_kolegija="${req.query.imeKolegija}") 
    AND id NOT IN (SELECT predavaonica_id FROM kolegij_predavaonica_zauzetost INNER JOIN zauzetost on zauzetost.id=kolegij_predavaonica_zauzetost.zauzetost_id WHERE
    datum_zauzetosti="${req.query.Datum}" AND vrijeme_kraja_zauzetosti > "${req.query.Pocetak}" AND vrijeme_pocetka_zauzetosti < "${req.query.Kraj}")`,
    (err, rows, fields) => {
        if (err) {
            throw err;
        } else {
            res.send(rows);
        }
    })
})
module.exports = router;
