const express= require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

router.get('/', (req, res) => {
    if(!req.query.Token){
            res.send('Unauthorized')
        }
        else{
            if(bcrypt.compareSync("helpmepls",req.query.Token))
                res.send('Authorized');
            else
                res.send('Unauthorized');
        }
})

module.exports = router;