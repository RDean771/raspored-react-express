const express= require('express');
const router = express.Router();
const conn = require('../../connection/connection');

router.get('/', (req, res) => {
    conn.query(`SELECT DISTINCT ime_studija FROM studij ORDER BY ime_studija`,(err,rows,fields)=>{
        if(err){
            throw err;
        } else{
            res.send(rows);
        }
    })
})

module.exports = router;